# CERN Accounts integration for OKD4 clusters

This project will do the necessary steps to provide the CERN Authentication/Authorization for users.

It assumes an application is already registered under [https://application-portal.web.cern.ch](https://application-portal.web.cern.ch) for the target OKD4 cluster.


### Troubleshooting

Everytime there is a change on the previous configuration, it's worth mentioning we must ensure the `oauth-openshift` pod is restarted and running again:

```bash
oc rollout status deployment oauth-openshift  -n openshift-authentication

# Sample output:
# Waiting for deployment "oauth-openshift" rollout to finish: 1 out of 2 new replicas have been updated...
# Waiting for deployment "oauth-openshift" rollout to finish: 1 out of 2 new replicas have been updated...
# Waiting for deployment "oauth-openshift" rollout to finish: 1 out of 2 new replicas have been updated...
# Waiting for deployment "oauth-openshift" rollout to finish: 1 old replicas are pending termination...
# Waiting for deployment "oauth-openshift" rollout to finish: 1 old replicas are pending termination...
# deployment "oauth-openshift" successfully rolled out
```

## E-group sync

This is application ensures that all CERN groups referenced in Openshift (Cluster-) RoleBindings are synced in our clusters.
As of April 2022, group memberships are retrieved from the [Authorization API](https://auth.docs.cern.ch/authzsvc/overview/).

The list of groups to sync is derived from groups referenced in namespace-scoped `RoleBinding`
and cluster-level `ClusterRoleBinding` resources.

This allows OKD project owners to just add a `Group` subject to their `RoleBinding`, and we automatically create an
Openshift `Group` synced with the corresponding CERN Group. We sync immediately as soon as we detect a new Openshift `Group`
reference in a `RoleBinding`; then we sync again every hour by default to propagate changes from the CERN Groups.

This mechanism also grants cluster-level permissions based on `ClusterRoleBinding`.

The application has the following components:

* The `egroup-usage-detector` deployment running 2 container (one for `RoleBindings` and one for `ClusterRoleBindings`):
  * A synchronous controller that listens to changes in `(Cluster-)RoleBindings` and creates an Openshift `Group` resource
    whenever a user specifies a new group in a `Rolebinding` resource.
    The controller creates the Openshift `Group` from a template (see `group.template` in `egroup-configmap.yaml`).
    This template sets labels and annotations on the new group so that it is marked as synced with Authz API.
* An `sync-egroup-membership` CronJob running every hour that synchronizes group memberships for **existing** Openshift Groups with the Authz API.

Notes of the Application sync behavior:
- Nested (Groups referencing other Groups) CERN Groups are expanded when syncronizing group memberships.
- A magic regex (see `get-nested-groups.sh`) is used to identify if a rolebinding subject is an CERN Group or not.
- This automatically excludes "internal" OpenShift groups like `system:...` from the sync.
- If the CERN group doesn't exist or is *blocked*, then the Openshift `Group` is still set up but remains empty.

For this purpose, read-only admin credentials for the AuthzAPI are required, since regular users cannot read all group memberships.

## Cluster admins permissions

This chart will provide OKD4 clusters with the users with the `cluster-admin` role.

To configure it, add a new username in the list under [`values.yaml`](chart/values.yaml):

```yaml
clusterAdmins:
  - <username>
```

## Kubeadmin deletion

FR: https://docs.openshift.com/container-platform/latest/authentication/remove-kubeadmin.html#removing-kubeadmin_removing-kubeadmin

This chart will delete the `kubeadmin` account from the OKD4 cluster.
