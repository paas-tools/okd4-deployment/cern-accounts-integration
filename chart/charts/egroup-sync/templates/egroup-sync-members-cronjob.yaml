# Periodically sync all OpenShift Groups that have previously been created by the "egroup-usage-detector"
apiVersion: batch/v1
kind: CronJob
metadata:
    name: sync-egroup-membership
    labels:
      app.kubernetes.io/name: sync-egroup-membership
spec:
    schedule: {{ .Values.cronjob.schedule }}
    concurrencyPolicy: Forbid
    successfulJobsHistoryLimit: 3
    failedJobsHistoryLimit: 3
    jobTemplate:
      metadata:
        labels:
          # this label is used further down in the 'command' to identify Jobs belonging to this CronJob
          app.kubernetes.io/name: sync-egroup-membership
      spec:
        # Dry-run with "worst case scenario" (Groups from PaaS + WebEOS+ Drupal together, ~1000) took 15min on WebEOS-proto cluster with AuthzAPI QA
        # But CPU steal can slow it down considerably and random timeouts/errors or temp unavailability of authz API could require the script to start over.
        # To avoid alarms due to job failures, allow the job to keep retrying for 90 minutes.
        # This requires:
        # - concurrencyPolicy=Forbid (so next execution is skipped if current one hasn't completed, since activeDeadlineSeconds may be > execution period)
        # - restartPolicy=OnFailure (for the container to restart with increasing back-off until it's successful)
        # - high value of backoffLimit, i.e. number of times the job's pod is allowed to restart after a failure.
        #   Due to CrashLoopBackoff behavior (https://stackoverflow.com/a/70717655) 90' allow for about 22 restarts max
        activeDeadlineSeconds: 5400
        backoffLimit: 20
        template:
          spec:
            nodeSelector:
              {{ .Values.cronjob.nodeSelector | toYaml | nindent 14 }}
            serviceAccount: egroup-sync
            containers:
            - name: egroup-sync
              image: {{ .Values.cronjob.image | quote }}
              command:
                - "/bin/bash"
                - "-xc"
                # Using '&&' ensures the commands only run when the previous command was successful
                - |
                  /etc/egroup-sync/get-nested-groups.sh 'sync-all' &&
                  /etc/egroup-sync/cleanup-failed-jobs.sh 'app.kubernetes.io/name=sync-egroup-membership'
              workingDir: "/tmp"
              {{- if .Values.dryRun }}
              env:
                - name: "DRY_RUN"
                  value: "true"
              {{- end }}
              envFrom:
                - secretRef:
                    name: {{ required "credentialsSecretName is required" .Values.credentialsSecretName }}
              volumeMounts:
              - mountPath: /etc/egroup-sync
                name: egroup-sync
            restartPolicy: OnFailure
            volumes:
            - configMap:
                defaultMode: 0751
                name: egroup-sync
              name: egroup-sync
