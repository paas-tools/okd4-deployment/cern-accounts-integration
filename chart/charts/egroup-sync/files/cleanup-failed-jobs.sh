#!/bin/bash

# Since the "sync-egroup-membership" is fully idempotent, we want to delete previous, failed invocation if the current invocation was successful.
# This avoids misleading "KubeJobFailed" alerts.

set -e

label_selector=$1
if [ -z "$label_selector" ]; then
    echo "Error: a label selector must be provided, e.g. cleanup-failed-jobs.sh my-label=my-value"
    exit 1
fi

OC_DRY_RUN_ARGS=
if [ -n "$DRY_RUN" ]; then
    echo "## Running in dry-run mode, no changes will be applied on the server"
    OC_DRY_RUN_ARGS="--dry-run=server"
fi

failed_jobs=$(oc get jobs -l "$label_selector" -o=jsonpath='{.items[?(@.status.failed)].metadata.name}')
if [ -n "$failed_jobs" ]; then
    oc delete jobs $failed_jobs $OC_DRY_RUN_ARGS
fi
