#!/bin/bash

# We set the process to stop if CURL fails to protect GROUPS for unexpected API responses
set -e

# Magic function that distuingished egroup names from "other" openshift groups
function looks_like_an_egroup {
  echo "$1" | grep -E '^[a-zA-Z0-9\-]+$'
}

function log {
    echo "[$(date --utc --iso-8601=seconds)]" "$@"
}

groupExistsInAuthzAPI(){
    local group=$1
    # Assess in the AuthzAPI that the $GROUP exists
    status=$(curl --fail --silent -X GET "$AUTHZAPI_URL/api/v1.0/Group/${group}" -H "accept: */*" -H "Authorization: Bearer ${BEARER_TOKEN}" -w '%{http_code}\n')
    if [[ $status == "404" ]]; then
        return 1
    fi
    return 0
}

# This function queries the AuthzAPI for a particular egroup and returns all the members in "${group-name}.json" (as an array).
# It also shows nested (recursive) group memberships.
# In case the egroup has been blocked, it returns an empty array.
getGroupMembers(){
    local group=$1
    # tmp_file will store the AuthzAPI complete response
    local tmp_file
    tmp_file=$(mktemp)
    # We curl the AuthzAPI according to their documentation, fetching Group members url: https://authorization-service-api.web.cern.ch/swagger/index.html
    # Most importantly:
    # - we use the '/recursive' endpoint to fetch nested groups
    # - we use the 'source:cern' filter to only fetch CERN members (i.e. no external or guest accounts)
    # - we ask the API to return only the 'upn' field (and nothing else)
    # - '--show-error' is used to log in case API call fails
    curl --fail --silent --show-error -X GET "${AUTHZAPI_URL}/api/v1.0/Group/${group}/members/identities/recursive?limit=${USERS_PER_REQUEST}&filter=source%3Acern&field=upn" -H "accept: */*" -H "Authorization: Bearer ${BEARER_TOKEN}" -o "${tmp_file}"
    # Append output to file
    cat "${tmp_file}" | jq -r '[.data[] | select(.upn != null and .upn != "") | .upn]' > "${group}.json"
    PAGE=$(cat "${tmp_file}" | jq -r '.pagination.next')
    # The API is paginated, thus we need to check for additional entries
    while [ "${PAGE}" != "null" ]; do
        curl --fail --silent --show-error -X GET "${AUTHZAPI_URL}${PAGE}" -H "accept: */*" -H "Authorization: Bearer ${BEARER_TOKEN}" > "${tmp_file}"
        #Get .upn from last CURL request and append to existing array
        cat "${tmp_file}" | jq -r '[.data[] | select(.upn != null and .upn != "") | .upn]' | jq -s add - ${group}.json > "tmp-${group}.json" ; cp tmp-${group}.json ${group}.json
        PAGE=$(cat "${tmp_file}" | jq -r '.pagination.next')
    done
    # Remove temporary files if they exist
    rm -f "${tmp_file}" tmp-${group}.json 2> /dev/null
}

processGroup(){
    local group=$1
    log "Processing group ${group}"
    # We fetch the group members into $GROUP.json, if $group does not exist, we path with empty list
    ## Blocked groups will return empty list of users
    echo '[]' > "${group}.json"
    ## Ignore groups that don't have the label indicating we should sync them
    if [[ -z $(oc get group -l "${group_label_selector}" --field-selector metadata.name="${group}" --ignore-not-found -o name) ]] ; then
        log "Group ${group} does not have label '${group_label_selector}', skipping"
        return
    fi
    if groupExistsInAuthzAPI "${group}"; then
        getGroupMembers "${group}"
    else
        # We must not delete the OKD group because if it's still referenced in a (cluster)rolebinding then it will be recreated and deleted again in a loop.
        # Just remove all members in this case.
        log "Group does not exist in AuthzAPI, removing all members of OKD group '${group}'"
    fi
    # We create a JSON patch file
    {
        echo '[{"op": "replace", "path": "/users","value":'
        cat "${group}.json"
        echo '}]'
    } > "${group}-patch.json"
    # Throw the patch against the API
    oc patch "group/${group}" --type json --patch-file "${group}-patch.json" ${OC_DRY_RUN_ARGS}
    # Delete unnecessary json, we keep ${group}-patch.json for possible debug scenario
    rm ${group}.json
    # We do not delete the $GROUP-patch file to enable debugging of the patch files, they will automatically be deleted along with the container
    # rm $GROUP-patch.json
}

# Function continuously watches a specified resource type, when changes are detected it will trigger a sync of the respective Groups
watchResource(){
  local resource_type=$1
  while true; do
      # .subjects[?(.kind=="Group")].name yields one of the following for each RoleBinding (or ClusterRoleBinding):
      # "<none> if no group are subject
      # "somegroup" if there's a single group subject
      # "group1,group2,group3" if there's multiple group subjects
      oc get "${resource_type}" --all-namespaces -o custom-columns='subjects:.subjects[?(.kind=="Group")].name' --no-headers --watch=true | while read -r subjects; do
        ensureValidToken
        # In case there is more than one Group in the same role, we split it into an array
        declare -a subjects=$(echo $subjects | tr ',' ' ')
        # not that we don't care about the (cluster)rolebinding name or anything, all we care about is if there's
        # a reference to a Group in the (cluster)rolebinding's subjects and that group name looks like an egroup name.
        for subject in ${subjects[@]}; do
            # We first check if the subject is a user or a group, if it is a group, we sync it immediately
            if looks_like_an_egroup "${subject}"; then
                # Confirm the Group exists in the API
                if groupExistsInAuthzAPI  "${subject}"; then
                    # Create group if it does not exist
                    if ! oc get group "${subject}"  >/dev/null 2>&1; then
                        # use the template to generate the Group resource (empty)
                        oc process --local -f /etc/egroup-sync/group-template.yaml GROUP_NAME="${subject}" | oc create -f - $OC_DRY_RUN_ARGS
                    fi
                    # Sync memberships of group
                    processGroup "${subject}"
                fi
            fi
        done
      done
      # clean up any dangling `oc` watch processes before restarting the loop
      pkill -15 -f "oc get .* --watch=true" || true
  done
}

# Tokens will expire after 24h+, we need to guarantee valid tokens, therefore if a token has been created more than 12h ago, we request a new token
ensureValidToken(){
    # According to the JWT provided by Keycloak, the expiration date per token is 20min as of 29/04/2022, so it's acceptable to refresh a few minutes before
    if [[ -z $(find . -name last-refreshed-timestamp -type f -mmin -15) ]]; then
        BEARER_TOKEN=$(curl --silent --fail -XPOST "${KC_ISSUER_URL}/api-access/token" -d "grant_type=client_credentials&client_id=${KC_CLIENT_ID}&client_secret=${KC_CLIENT_SECRET}&audience=authorization-service-api" | jq -r '.access_token')
        touch last-refreshed-timestamp
        log "API Token refreshed"
    fi
}

usage() {
    echo "Usage: $0 [sync-all] | [watch <RESOURCE_TYPE>] | [sync <GROUP_NAME>]"
    echo "The following environment variables have to be set: AUTHZAPI_URL, KC_ISSUER_URL, KC_CLIENT_ID, KC_CLIENT_SECRET";
    echo "Set DRY_RUN enviroment variable to print out changes instead of sending them to Openshift API;"
    exit 1;
}

## EXPECTED INPUT
INPUT=$1
### Number of entries per request on the AuthzAPI, current value is the official limit by the API
USERS_PER_REQUEST="1000"

# only sync groups matching this label selector
group_label_selector='okd.cern.ch/sync.host=cern-authz-api'

[[ -z "$AUTHZAPI_URL"  || -z "${KC_ISSUER_URL}" || -z "${KC_CLIENT_ID}" || -z "${KC_CLIENT_SECRET}" ]] && usage
rm -f last-refreshed-timestamp
ensureValidToken


OC_DRY_RUN_ARGS=
if [ -n "$DRY_RUN" ]; then
    echo "## Running in dry-run mode, no changes will be applied on the server"
    OC_DRY_RUN_ARGS="-o yaml --dry-run=server"
fi

if [[ $INPUT == "sync-all" ]]; then
    # Iterate through all the GROUPS found in the cluster with label indicating they should be synced
    egroups=$(oc get group -o jsonpath='{.items[*].metadata.name}' -l "${group_label_selector}")
    for group in $egroups; do
        ensureValidToken
        processGroup "${group}"
    done
elif [[ $INPUT == "watch" ]]; then
    resource_type=$2
    if [[ -z ${resource_type} ]]; then
        usage
    fi
    watchResource "${resource_type}"
elif [[ $INPUT == "sync" ]]; then
    groupName=$2
    processGroup "${groupName}"
else
    usage
fi
